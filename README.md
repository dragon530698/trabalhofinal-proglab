# TrabalhoFinal-ProgLab

Projeto dedicado ao trabalho final da matéria Laboratório de Programação (IFSC - CC05 2021.2)
**Aluno:** João Pedro Magalhães

# Observações
**O projeto faz conexão com um Banco de Dados.** (PostgreSQL 13);
Os parametros de configuração estão localizados em:

```
\src\main\java\ifsc\proglab\avaliacao2\sistemadevendas\db.properties
```