package ifsc.proglab.avaliacao2.sistemadevendas.Controller;

import ifsc.proglab.avaliacao2.sistemadevendas.DAO.*;
import ifsc.proglab.avaliacao2.sistemadevendas.Model.Product;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Formatter;
import org.javatuples.Pair;

public class AppViewController {
    private final ProductDAO pDAO = new ProductDAO();
    private final CartDAO cDAO = new CartDAO();
    private final FileManager fileMngr = new FileManager();
    
    public ArrayList<Product> GetAllProducts()
    {
        return pDAO.AllProducts();
    }
    
    public ArrayList<Pair<Product, Integer>> GetCartItems()
    {
        return cDAO.getCurrentCart().getProductsList();
    }
    
    public float GetCartTotalPrice()
    {
        return cDAO.getCurrentCart().getTotalPrice();
    }
    
    public void AddProductToCart(Product product, int quantity)
    {
        cDAO.AddProductToCart(product, quantity);
    }
    
    public void RemoveProductFromCart(Product product)
    {
        cDAO.RemoveProductFromCart(product);
    }
    
    public void ClearCart()
    {
        cDAO.ClearCart();
    }
    
    public void ExportToTxt()
    {
        Formatter formatter = new Formatter();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        
        String str = "Mercadinho Magalhães\n";
        str = str + dtf.format(LocalDateTime.now()) + "\n\n\n";
    
        str = str + formatter.format("|%1$-5s|%2$-30s|%3$-5s|%4$-13s|%5$-13s|\n",
                "ID", "Produto", "Qtd.", "Valor Un.", "Valor Total").toString();
        str = str + "------------------------------------------------------------------------\n";
        str = str + cDAO.getCurrentCart().toString();
        str = str + "------------------------------------------------------------------------\n";
        
        fileMngr.ToTxt(str);
    }
}
