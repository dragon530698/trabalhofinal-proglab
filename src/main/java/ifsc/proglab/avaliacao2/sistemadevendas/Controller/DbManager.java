package ifsc.proglab.avaliacao2.sistemadevendas.Controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DbManager 
{
    // Singleton setup
    private static DbManager _instance = null;
    
    public static DbManager Instance()
    {
        synchronized (DbManager.class)
        {
            if (_instance == null)
            {
                _instance = new DbManager();
            }
        }
        
        return _instance;
    }
    
    private Connection conn;
    
    private void ConnectToDB() throws SQLException, FileNotFoundException, IOException
    {
        FileReader reader = new FileReader(Paths.get("src/main/java/ifsc/proglab/avaliacao2/sistemadevendas/db.properties").toAbsolutePath().toString());
        
        Properties props = new Properties();
        props.load(reader);
        
        conn = DriverManager.getConnection(props.getProperty("url"), props);
        System.out.println("Connection to database succeeded!!");
    }
    
    public Connection Connection()
    {   
        try
        {
            if (conn == null)
            {
                ConnectToDB();
                return conn;
            }
            
            if (conn.isValid(30))
            {
                return conn;
            }
            else if (conn.isClosed())
            {
                ConnectToDB();
                return conn;
            }
        }
        catch(SQLException ex)
        {
            System.out.println("Failed to connect to database!");
            System.out.println(ex.getMessage());
            return null;
        }
        catch(FileNotFoundException ex)
        {
            System.out.println("Failed to find database settings!");
            System.out.println(ex.getMessage());
            return null;
        }
        catch(IOException ex)
        {
            System.out.println("Failed to read database settings file!");
            System.out.println(ex.getMessage());
            return null;
        }
        
        System.out.println("Unexpected behaviour on attempt to connect to database!");
        return null;
    }

    public boolean SetupTables()
    {
        try
        {
            if (conn == null)
            {
                int tries = 0;
                while (conn == null && tries < 5)
                {
                    Connection();
                    tries++;
                }
                
                if (tries == 5)
                {
                        System.out.println("Failed to connect to DB after 5 tries!");
                        System.out.println("Canceling table creation.");
                        return false;
                }
            }

            String productTableSql =
                    "CREATE TABLE IF NOT EXISTS "
                    + conn.getSchema() + ".products"
                    + "("
                    + "id SERIAL PRIMARY KEY,"
                    + "name VARCHAR(255) UNIQUE NOT NULL,"
                    + "price DECIMAL NOT NULL"
                    + ");";

            Statement stm = conn.createStatement();
            stm.executeUpdate(productTableSql);

            return true;
        }
        catch (SQLException ex)
        {
            System.out.println("Failed to setup database tables!");
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
