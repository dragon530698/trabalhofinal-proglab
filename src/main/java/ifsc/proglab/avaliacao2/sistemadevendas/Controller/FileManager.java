/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifsc.proglab.avaliacao2.sistemadevendas.Controller;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Jaozin
 */
public class FileManager {
    public void ToTxt(String content)
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm");
        
        String fileName = "Carrinho_" + dtf.format(LocalDateTime.now()) +  ".txt";
        
        try
        {
            FileWriter writer = new FileWriter(Paths.get(fileName).toAbsolutePath().toString());
            writer.write(content);
            writer.close();
        }
        catch (IOException ex)
        {
            System.out.println("Failed to write to export to txt!");
            System.out.println(ex.getMessage());
        }
    }
}
