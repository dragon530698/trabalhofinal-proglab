package ifsc.proglab.avaliacao2.sistemadevendas.DAO;

import ifsc.proglab.avaliacao2.sistemadevendas.Model.Cart;
import ifsc.proglab.avaliacao2.sistemadevendas.Model.Product;
import java.util.ArrayList;
import org.javatuples.Pair;

public class CartDAO {
    private Cart currentCart = new Cart();
    
    public void AddProductToCart(Product product, int quantity)
    {
        ArrayList<Pair<Product, Integer>> productsList = currentCart.getProductsList();
        
        for(Pair<Product, Integer> entry : productsList)
        {
            if (entry.getValue0() == product)
            {
                float totalPrice = currentCart.getTotalPrice();
                totalPrice += product.getPrice() * quantity;
                currentCart.setTotalPrice(totalPrice);
                
                Pair<Product, Integer> newEntry = new Pair<>(entry.getValue0(), entry.getValue1() + quantity);
                productsList.set(productsList.indexOf(entry), newEntry);
                
                return;
            }
        }
        
        Pair<Product, Integer> productEntry = new Pair<>(product, quantity);
        
        productsList.add(productEntry);
        
        float totalPrice = currentCart.getTotalPrice();
        totalPrice += product.getPrice() * quantity;
        currentCart.setTotalPrice(totalPrice);
    }
    
    public void RemoveProductFromCart(Product product)
    {
        ArrayList<Pair<Product, Integer>> productsList = currentCart.getProductsList();
        
        boolean entryRemoved = false;
        
        for(Pair<Product, Integer> entry : productsList)
        {
            if (entry.getValue0() == product)
            {
                float totalPrice = currentCart.getTotalPrice();
                totalPrice -= entry.getValue0().getPrice() * entry.getValue1();
                currentCart.setTotalPrice(totalPrice);
                
                productsList.remove(entry);
                entryRemoved = true;
                return;
            }
        }
    }

    public void ClearCart()
    {
        currentCart.getProductsList().clear();
        currentCart.setTotalPrice(0);
    }

    public Cart getCurrentCart() {
        return currentCart;
    }

    public void setCurrentCart(Cart currentCart) {
        this.currentCart = currentCart;
    }
}
