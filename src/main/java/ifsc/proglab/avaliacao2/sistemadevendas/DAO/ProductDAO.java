package ifsc.proglab.avaliacao2.sistemadevendas.DAO;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ifsc.proglab.avaliacao2.sistemadevendas.Controller.DbManager;
import ifsc.proglab.avaliacao2.sistemadevendas.Model.Product;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {
    public boolean Insert(Product p)
    {
        try
        {
            Connection conn = DbManager.Instance().Connection();
            
            String sql = "INSERT INTO " + conn.getSchema()
                    + ".products(name,price) "
                    + "VALUES("
                    + "'" + p.getName() + "', "
                    + p.getPrice()
                    + ");";
            
            Statement stm = conn.createStatement();
            stm.executeUpdate(sql);
            return true;
        }
        catch (SQLException ex)
        {
            System.out.println("Error while executing INSERT statement!");
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public boolean Delete(Product p)
    {
        try
        {
            Connection conn = DbManager.Instance().Connection();
            
            String sql = "DELETE FROM " + conn.getSchema()+ ".products "
                    + "WHERE id = " + p.getId()
                    + ";";
            
            Statement stm = conn.createStatement();
            stm.executeUpdate(sql);
            
            return true;
        }
        catch (SQLException ex)
        {
            System.out.println("Error while executing DELETE statement!");
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public boolean Update(Product p)
    {
        // TODO: Add comparison between value in DB and in system
        // ALTERNATIVE: Dirty flags?
        try
        {
            Connection conn = DbManager.Instance().Connection();
            
            String sql = "UPDATE " + conn.getSchema()+ ".products "
                    + "SET name = '" + p.getName() + "', "
                    + "price = " + p.getPrice() + " "
                    + "WHERE id = " + p.getId()
                    + ";";
            
            Statement stm = conn.createStatement();
            stm.executeUpdate(sql);
            
            return true;
        }
        catch (SQLException ex)
        {
            System.out.println("Error while executing UPDATE statement!");
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public ArrayList<Product> AllProducts()
    {
        ArrayList<Product> query = new ArrayList();
        
        try
        {
            Connection conn = DbManager.Instance().Connection();
            
            String sql = "SELECT * FROM " + conn.getSchema() + ".products;";
            
            Statement stm = conn.createStatement();
            ResultSet result = stm.executeQuery(sql);
            
            while (result.next())
            {
                Product p = new Product();
                p.setId(result.getInt("id"));
                p.setName(result.getString("name"));
                p.setPrice(result.getFloat("price"));
                query.add(p);
            }
            
            return query;
        }
        catch (SQLException ex)
        {
            System.out.println("Error while executing SELECT statement!");
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public boolean PopulateDb()
    {
        try
        {
            Connection conn = DbManager.Instance().Connection();
            FileReader reader = new FileReader(
                    Paths.get(
                            "src/main/java/ifsc/proglab/avaliacao2/sistemadevendas/allProducts.json"
                    ).toAbsolutePath().toString());
            
            Type listType = new TypeToken<List<Product>>() {}.getType();

            
            Gson g = new Gson();
            List<Product> defaultProducts = g.fromJson(reader, listType);
            
            String sql = "INSERT INTO " + conn.getSchema() + ".products(name,price) VALUES ";
            
            int i = 0;
            for (Product p : defaultProducts)
            {
                i++;
                if (i == defaultProducts.size())
                {
                    sql = sql + "("
                    + "'" + p.getName() + "', "
                    + p.getPrice()
                    + ") ";
                }
                else if (i < defaultProducts.size())
                {
                    sql = sql + "("
                    + "'" + p.getName() + "', "
                    + p.getPrice()
                    + "), ";
                }
            }
            
            sql = sql + "ON CONFLICT (name) DO NOTHING;";
            
            Statement stm = conn.createStatement();
            stm.executeUpdate(sql);
            return true;
        }
        catch (IOException ex)
        {
            System.out.println("Failed to read products file!");
            System.out.println(ex.getMessage());
            return false;
        }
        catch( SQLException ex)
        {
            System.out.println("Failed to populate database products table!");
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
