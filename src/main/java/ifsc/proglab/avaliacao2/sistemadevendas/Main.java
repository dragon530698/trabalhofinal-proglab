package ifsc.proglab.avaliacao2.sistemadevendas;

import ifsc.proglab.avaliacao2.sistemadevendas.Controller.DbManager;
import ifsc.proglab.avaliacao2.sistemadevendas.DAO.ProductDAO;
import ifsc.proglab.avaliacao2.sistemadevendas.View.AppView;

public class Main {
    
    public static void main(String[] args)
    {
        SetupDB();
        DisplayGUI();
    }
    
    private static void SetupDB()
    {
        if (DbManager.Instance().SetupTables())
        {
            ProductDAO pDAO = new ProductDAO();
            
            if (pDAO.PopulateDb())
            {
                System.out.println("DB populated successfully!");
            }
            else
            {
                System.out.println("Failed to populate DB!");
            }
        }
        else
        {
            System.out.println("Failed to create tables!");
        }
    }

    private static void DisplayGUI()
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AppView().setVisible(true);
            }
        });
    }
}

