package ifsc.proglab.avaliacao2.sistemadevendas.Model;

import java.util.ArrayList;
import java.util.Formatter;
import org.javatuples.Pair;

public class Cart {
    private ArrayList<Pair<Product, Integer>> productsList = new ArrayList<>();
    private float totalPrice = 0;
    
    public Cart() {};

    @Override
    public String toString()
    {
        String str = "";
        
        for (Pair<Product, Integer> entry : productsList)
        {
            Formatter formatter = new Formatter();
            String strToAdd = formatter.format("|%1$-5s|%2$-30s|%3$-5s|%4$-13s|%5$-13s|\n",
                    (productsList.indexOf(entry) + 1),
                    entry.getValue0().getName(),
                    entry.getValue1(),
                    entry.getValue0().getPrice(),
                    entry.getValue0().getPrice() * entry.getValue1()).toString();
            
            str = str + strToAdd;
        }
        
        return str;
    }
    
    public ArrayList<Pair<Product, Integer>> getProductsList() {
        return productsList;
    }

    public void setProductsList(ArrayList<Pair<Product, Integer>> productsList) {
        this.productsList = productsList;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }
}
