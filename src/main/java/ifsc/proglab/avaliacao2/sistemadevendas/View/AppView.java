package ifsc.proglab.avaliacao2.sistemadevendas.View;

import ifsc.proglab.avaliacao2.sistemadevendas.Controller.AppViewController;
import ifsc.proglab.avaliacao2.sistemadevendas.Model.Product;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import org.javatuples.Pair;


public class AppView extends javax.swing.JFrame {
    public AppView() {
        initComponents();
        
        LoadProducts();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        pdfExportBtn = new javax.swing.JButton();
        txtExportBtn = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        clearCartBtn = new javax.swing.JButton();
        removeFromCartBtn = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        cartTableScrollPane = new javax.swing.JScrollPane();
        cartTable = new javax.swing.JTable() {
            public boolean isCellEditable(int rowIndex, int colIndex)
            {
                return false;
            }
        };
        jPanel7 = new javax.swing.JPanel();
        cartTotalPriceLabel = new javax.swing.JLabel();
        cartTotalValueLabelTitle = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        quantityInput = new javax.swing.JSpinner();
        addToCartBtn = new javax.swing.JButton();
        productsTableScrollPane = new javax.swing.JScrollPane();
        productsTable = new javax.swing.JTable() {
            public boolean isCellEditable(int rowIndex, int colIndex)
            {
                return false;
            }
        };

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        pdfExportBtn.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        pdfExportBtn.setText("PDF");
        pdfExportBtn.setEnabled(false);
        pdfExportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfExportBtnActionPerformed(evt);
            }
        });
        jSplitPane1.setLeftComponent(pdfExportBtn);

        txtExportBtn.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        txtExportBtn.setText("TXT");
        txtExportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtExportBtnActionPerformed(evt);
            }
        });
        jSplitPane1.setRightComponent(txtExportBtn);

        clearCartBtn.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        clearCartBtn.setText("Limpar Carrinho");
        clearCartBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearCartBtnActionPerformed(evt);
            }
        });

        javax.swing.ImageIcon minusIcon = new javax.swing.ImageIcon(Paths.get("src/main/java/ifsc/proglab/avaliacao2/sistemadevendas/minusIcon.png").toAbsolutePath().toString());
        java.awt.Image minusIconImg = minusIcon.getImage();
        java.awt.Image resizedMinusIcon = minusIconImg.getScaledInstance(35, 35, java.awt.Image.SCALE_SMOOTH);
        removeFromCartBtn.setBackground(javax.swing.UIManager.getDefaults().getColor("window"));
        removeFromCartBtn.setForeground(javax.swing.UIManager.getDefaults().getColor("window"));
        removeFromCartBtn.setIcon(new javax.swing.ImageIcon(resizedMinusIcon));
        removeFromCartBtn.setBorder(null);
        removeFromCartBtn.setBorderPainted(false);
        removeFromCartBtn.setEnabled(false);
        removeFromCartBtn.setFocusPainted(false);
        removeFromCartBtn.setPreferredSize(new java.awt.Dimension(40, 40));
        removeFromCartBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeFromCartBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(removeFromCartBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clearCartBtn)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removeFromCartBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(clearCartBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(68, Short.MAX_VALUE))
        );

        jLabel3.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        jLabel3.setText("Carrinho");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel4.setFont(new java.awt.Font("Courier New", 3, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel4.setText("Mercadinho Magalhães");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        cartTable.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        cartTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Produto", "Quantidade", "Valor Unitário", "Valor Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        cartTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        cartTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        cartTable.setShowGrid(false);
        cartTable.setShowHorizontalLines(true);
        cartTable.getSelectionModel().addListSelectionListener(new javax.swing.event.ListSelectionListener()
            {
                public void valueChanged(javax.swing.event.ListSelectionEvent event)
                {
                    cartTableValueChanged(event);
                }
            });
            cartTableScrollPane.setViewportView(cartTable);

            cartTotalPriceLabel.setFont(new java.awt.Font("Courier New", 0, 24)); // NOI18N
            cartTotalPriceLabel.setText("R$0,00");

            cartTotalValueLabelTitle.setFont(new java.awt.Font("Courier New", 0, 24)); // NOI18N
            cartTotalValueLabelTitle.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
            cartTotalValueLabelTitle.setText("Total:");

            javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
            jPanel7.setLayout(jPanel7Layout);
            jPanel7Layout.setHorizontalGroup(
                jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(cartTotalValueLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cartTotalPriceLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
            );
            jPanel7Layout.setVerticalGroup(
                jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cartTotalPriceLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cartTotalValueLabelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
            );

            jLabel5.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
            jLabel5.setText("Exportar:");

            javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
            jPanel1.setLayout(jPanel1Layout);
            jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cartTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 701, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(4, 4, 4)
                            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
            );
            jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cartTableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(6, 6, 6))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addContainerGap())))
            );

            jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

            jLabel1.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
            jLabel1.setText("Produtos");

            javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
            jPanel4.setLayout(jPanel4Layout);
            jPanel4Layout.setHorizontalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 180, Short.MAX_VALUE))
            );
            jPanel4Layout.setVerticalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
            );

            jLabel2.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
            jLabel2.setText("Quantidade");

            quantityInput.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
            quantityInput.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

            javax.swing.ImageIcon plusIcon = new javax.swing.ImageIcon(Paths.get("src/main/java/ifsc/proglab/avaliacao2/sistemadevendas/plusIcon.png").toAbsolutePath().toString());
            java.awt.Image plusImgIcon = plusIcon.getImage();
            java.awt.Image resizedPlusIcon = plusImgIcon.getScaledInstance(35, 35, java.awt.Image.SCALE_SMOOTH);
            addToCartBtn.setBackground(javax.swing.UIManager.getDefaults().getColor("window"));
            addToCartBtn.setForeground(javax.swing.UIManager.getDefaults().getColor("window"));
            addToCartBtn.setIcon(new javax.swing.ImageIcon(resizedPlusIcon));
            addToCartBtn.setBorder(null);
            addToCartBtn.setEnabled(false);
            addToCartBtn.setPreferredSize(new java.awt.Dimension(40, 40));
            addToCartBtn.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    addToCartBtnActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
            jPanel8.setLayout(jPanel8Layout);
            jPanel8Layout.setHorizontalGroup(
                jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel2)
                    .addGap(6, 6, 6)
                    .addComponent(quantityInput, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(addToCartBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
            );
            jPanel8Layout.setVerticalGroup(
                jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(quantityInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addComponent(addToCartBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(24, Short.MAX_VALUE))
            );

            productsTable.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
            productsTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {

                },
                new String [] {
                    "Produto", "Preço"
                }
            ) {
                Class[] types = new Class [] {
                    java.lang.Object.class, java.lang.Float.class
                };

                public Class getColumnClass(int columnIndex) {
                    return types [columnIndex];
                }
            });
            productsTable.setToolTipText("");
            productsTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
            productsTable.setMinimumSize(new java.awt.Dimension(5, 0));
            productsTable.setRequestFocusEnabled(false);
            productsTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
            productsTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
            productsTable.setShowGrid(false);
            productsTable.setShowHorizontalLines(true);
            productsTable.getSelectionModel().addListSelectionListener(new javax.swing.event.ListSelectionListener()
                {
                    public void valueChanged(javax.swing.event.ListSelectionEvent event)
                    {
                        productsTableValueChanged(event);
                    }
                });
                productsTableScrollPane.setViewportView(productsTable);

                javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
                jPanel3.setLayout(jPanel3Layout);
                jPanel3Layout.setHorizontalGroup(
                    jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(productsTableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                );
                jPanel3Layout.setVerticalGroup(
                    jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(productsTableScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                );

                javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
                jPanel2.setLayout(jPanel2Layout);
                jPanel2Layout.setHorizontalGroup(
                    jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
                jPanel2Layout.setVerticalGroup(
                    jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                );

                pack();
            }// </editor-fold>//GEN-END:initComponents

    private AppViewController controller = new AppViewController();
    
    private void LoadProducts()
    {
        DefaultTableModel model = (DefaultTableModel) productsTable.getModel();
        ArrayList<Product> products = controller.GetAllProducts();
        
        for (Product p : products)
        {
            Object[] entryToAdd = new Object[2];
            entryToAdd[0] = p;
            entryToAdd[1] = p.getPrice();
            
            model.addRow(entryToAdd);
        }
    }
    
    private void ReloadCart()
    {
        DefaultTableModel model = (DefaultTableModel) cartTable.getModel();
        ArrayList<Pair<Product, Integer>> cartItems = controller.GetCartItems();
        int i = 1;
        
        model.setRowCount(0);
        
        for (Pair<Product, Integer> entry : cartItems)
        {
            Product p = entry.getValue0();
            
            Object[] entryToAdd = new Object[5];
            entryToAdd[0] = i;
            entryToAdd[1] = p;
            entryToAdd[2] = entry.getValue1();
            entryToAdd[3] = p.getPrice();
            entryToAdd[4] = p.getPrice() * entry.getValue1();
            
            model.addRow(entryToAdd);
            i++;
        }
        
        DecimalFormat df = new DecimalFormat("0.00");
        cartTotalPriceLabel.setText("R$" + df.format(controller.GetCartTotalPrice()));
    }
    
    private void pdfExportBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfExportBtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pdfExportBtnActionPerformed

    private void addToCartBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addToCartBtnActionPerformed
        if (productsTable.getSelectedRow() != -1)
        {
            Product selectedProduct = (Product) productsTable.getModel().getValueAt(productsTable.getSelectedRow(), 0);
            int quantity = (Integer) quantityInput.getValue();
            
            controller.AddProductToCart(selectedProduct, quantity);
            ReloadCart();
            
            quantityInput.setValue(1);
        }
    }//GEN-LAST:event_addToCartBtnActionPerformed

    private void clearCartBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearCartBtnActionPerformed
        DefaultTableModel model = (DefaultTableModel) cartTable.getModel();
        model.setRowCount(0);
        controller.ClearCart();
        
        cartTotalPriceLabel.setText("R$0,00");
    }//GEN-LAST:event_clearCartBtnActionPerformed

    private void removeFromCartBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeFromCartBtnActionPerformed
        if (cartTable.getSelectedRow() != -1)
        {
            Product selectedProduct = (Product) cartTable.getModel().getValueAt(cartTable.getSelectedRow(), 1);
            
            controller.RemoveProductFromCart(selectedProduct);
            ReloadCart();
        }
    }//GEN-LAST:event_removeFromCartBtnActionPerformed

    private void txtExportBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtExportBtnActionPerformed
        controller.ExportToTxt();
    }//GEN-LAST:event_txtExportBtnActionPerformed

    private void productsTableValueChanged(javax.swing.event.ListSelectionEvent event)
    {
        if (productsTable.getSelectedRow() != -1)
        {
            addToCartBtn.setEnabled(true);
        }
        else if (productsTable.getSelectedRow() == -1)
        {
            addToCartBtn.setEnabled(false);
        }
    }
    
    private void cartTableValueChanged(javax.swing.event.ListSelectionEvent event)
    {
        if (cartTable.getSelectedRow() != -1)
        {
            removeFromCartBtn.setEnabled(true);
        }
        else if (cartTable.getSelectedRow() == -1)
        {
            removeFromCartBtn.setEnabled(false);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addToCartBtn;
    private javax.swing.JTable cartTable;
    private javax.swing.JScrollPane cartTableScrollPane;
    private javax.swing.JLabel cartTotalPriceLabel;
    private javax.swing.JLabel cartTotalValueLabelTitle;
    private javax.swing.JButton clearCartBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JButton pdfExportBtn;
    private javax.swing.JTable productsTable;
    private javax.swing.JScrollPane productsTableScrollPane;
    private javax.swing.JSpinner quantityInput;
    private javax.swing.JButton removeFromCartBtn;
    private javax.swing.JButton txtExportBtn;
    // End of variables declaration//GEN-END:variables
}
